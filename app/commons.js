const appSettings = require("tns-core-modules/application-settings");

// Audio

const audio = require('nativescript-audio');

function playSuccess() {
  if (appSettings.getBoolean('settings.enable_sounds', true)) {
    const success_player = new audio.TNSPlayer();
    success_player.initFromFile({
      audioFile: '~/assets/audio/skan.mp3',
      loop: false,
    });

    return success_player.play()
  }

  return Promise.resolve()
}


function playError() {
  if (appSettings.getBoolean('settings.enable_sounds', true)) {
    const error_player = new audio.TNSPlayer();
    error_player.initFromFile({
      audioFile: '~/assets/audio/eraro.mp3',
      loop: false,
    });

    return error_player.play()
  }

  return Promise.resolve()
}


export const sysAudio = {
  playSuccess: playSuccess,
  playError: playError,
};


// Avatar
export const AvatarSource = {
  Camera: 'camera',
  Image: 'image',
};

export const selectAvatar = function (source = AvatarSource.Image) {
  const ImageSource = require('tns-core-modules/image-source').ImageSource;
  const ImageCropper = require('nativescript-imagecropper').ImageCropper;

  if (source === AvatarSource.Image) {
    const ImagePicker = require('nativescript-imagepicker');
    const imagePicker = ImagePicker.create({mode: "single", mediaType: ImagePicker.ImagePickerMediaType.Image});

    const res = imagePicker.authorize()
      .then(() => {
        const result = imagePicker.present()
          .then(select => {
            if (select.length) {
              const selected = select[0];
              let source = new ImageSource();

              const pict = source.fromAsset(selected)
                .then(picture => {
                  let imageCropper = new ImageCropper();

                  const cropped_pict = imageCropper.show(picture, {width: 640, height: 640, lockSquare: true})
                    .then(args => {
                      if (args.image) {
                        return Promise.resolve(args.image)
                      }
                      return Promise.reject()
                    })
                    .catch(e => {
                      return Promise.reject(e)
                    });

                  return cropped_pict
                });

              return pict
            }

            return Promise.reject()
          })
          .catch(e => {
            return Promise.reject()
          });

        return result
      })
      .catch(e => {
        return Promise.reject(e)
      });

    return Promise.resolve(res)

  } else if (source === AvatarSource.Camera) {
    const camera = require('nativescript-camera');

    const res = camera.requestCameraPermissions()
      .then(() => {
        const result = camera.takePicture({saveToGallery: false, keepAspectRatio: true})
          .then(imageAsset => {
            let source = new ImageSource();

            const pict = source.fromAsset(imageAsset)
              .then(picture => {
                let imageCropper = new ImageCropper();

                const cropped_pict = imageCropper.show(picture, {width: 640, height: 640, lockSquare: true})
                  .then(args => {
                    if (args.image) {
                      return Promise.resolve(args.image)
                    }
                    return Promise.reject()
                  })
                  .catch(e => {
                    return Promise.reject()
                  });

                return cropped_pict
              });

            return pict
          })
          .catch(e => {
            return Promise.reject()
          });

        return result
      });

    return res
  }

  return Promise.reject()
};
